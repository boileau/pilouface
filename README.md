# Pile ou Face

Une étude des séquences aléatoires sous forme de notebooks Jupyter :

- version statique html : <https://boileau.pages.math.unistra.fr/pilouface/>
- version exécutable : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fboileau%2Fpilouface/master?filepath=PileOuFace.ipynb)