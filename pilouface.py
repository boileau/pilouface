"""
Calculer la fréquence d'apparition d'une séquence consécutive de pile
(ou de face) dans un tirage aléatoire
"""

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rcParams, cm, animation
import matplotlib.colors as mcolors
import pandas as pd
from IPython.display import HTML as html_print
from IPython.display import display
from numba import jit
from progressbar import progressbar

rcParams['figure.figsize'] = (8., 6.)  # Enlarge figure


def color_sequence(seq: str, strong=False) -> str:
    """return a color version of sequence seq"""

    def color_seq(seq: np.ndarray, strong_char: np.ndarray) -> str:
        """return a html color version of seq"""

        def cstr(s: str, strongify) -> str:
            """return a html color or bold version of F or P"""
            color = 'blue' if s == 'P' else 'red'
            if strongify:
                color = 'dark' + color
                s = f"<strong>{s}</strong>"
            return f"<text style=color:{color}>{s}</text>"

        return ''.join([cstr(seq[i], strong_char[i]) for i in range(len(seq))])

    # An array for longest sequence position
    strong_char = np.full((len(seq), ), False)
    if strong:
        # Set True where longest sequence is found
        for select in 'P', 'F':
            long_seq, istart = get_longest_sequence(str2ndarray(seq),
                                                    select=select)
            strong_char[istart:istart + long_seq] = True

    return html_print(color_seq(seq, strong_char) + f" [{len(seq)}]")


def str2ndarray(s: str) -> np.ndarray:
    """
    Convert FPPFFPF into array of bool
    """
    str_arr = np.fromiter(s, (np.unicode, 1))
    return np.where(str_arr == 'P', True, False)


@jit(nopython=True)
def get_longest_sequence(sample: np.ndarray, select='P') -> tuple:
    """Return a tuple of the longest sequence and its position"""
    choice = (select == 'P')  # 'P': True, 'F': False
    long_seq = 0
    new_seq = 0
    i_last = 0
    for i in range(sample.shape[0]):
        if sample[i] == choice:
            new_seq += 1
            if new_seq > long_seq:
                long_seq = new_seq
                i_last = i
        else:
            new_seq = 0
    return long_seq, i_last - long_seq + 1


class PileOuFace:

    def __init__(self, nsample=10000, length=100):

        self.length = length
        self.nsample = nsample
        self.long_pile = np.empty((self.nsample, ), dtype='int')
        self.long_face = np.empty((self.nsample, ), dtype='int')
        self.df = pd.DataFrame()
        self.name = f"{self.nsample} tirages aléatoires"
        self.mean = None
        self.std = None

    def compute_sequences(self):
        """Compute the longuest sequences for nsample samples"""
        print(f"Computing {self.nsample} samples")

        for i in progressbar(range(self.nsample)):
            # a random array of boolean for face or pile
            sample = np.random.choice(a=[False, True], size=self.length)
            self.long_pile[i], _ = get_longest_sequence(sample, select='P')
            self.long_face[i], _ = get_longest_sequence(sample, select='F')

    def get_bins(self) -> np.ndarray:
        """Return the maximum number of bins between pile and face"""
        stop = max(self.long_pile.max(), self.long_face.max())
        return np.arange(start=-0.5, stop=stop, step=1)

    def get_histogram1D(self) -> pd.DataFrame:

        def get_one_histo(seq):
            return np.histogram(seq, bins=self.get_bins(), density=True)

        histo_pile = get_one_histo(self.long_pile)
        histo_face = get_one_histo(self.long_face)
        return pd.DataFrame({f'Pile': histo_pile[0],
                             f'Face': histo_face[0]})

    def get_histogram2D(self) -> pd.DataFrame:

        def get_one_histo(seq):
            return np.histogram(seq, bins=self.get_bins(), density=True)

        histo2d = np.histogram2d(self.long_pile, self.long_face, density=True,
                                 bins=self.get_bins())[0]
        return pd.DataFrame(histo2d)

    def set_title(self, ax):
        title = f"Suite de {self.length} éléments, " + \
                f"{self.nsample} échantillons"
        ax.set_title(title, fontsize=10)

    def plot1D(self):
        """Plot 1D histogram"""

        fig = plt.figure()
        ax = fig.add_subplot(111)

        bins = self.get_bins()

        def plot_seq(long_seqs, **kwargs):

            ps = pd.Series(long_seqs)
            ax = ps.plot.hist(bins=bins, density=True, align='mid', alpha=1.,
                              **kwargs)
            ax.set_xlabel("Longueur de la séquence")
            ax.set_ylabel("Probabilité d'apparition")
            ax.set_xticks(bins + 0.5)
            ax.set_xlim([0, 20])
            suptitle = "Histogramme de la séquence la plus longue"
            fig.suptitle(suptitle, fontsize=14)
            self.set_title(ax)
            ax.legend()

        plot_seq(self.long_pile, label="Pile", rwidth=0.5)
        plot_seq(self.long_face, label="face", rwidth=0.25)

        return fig

    def plot2D(self):
        """Plot 2D histogram"""

        fig = plt.figure()
        ax = fig.add_subplot(111)

        max_length = 14
        ticks = range(max_length)
        bins = self.get_bins()

        cmap = 'GnBu'
        histo2d = ax.hist2d(self.long_pile, self.long_face, density=True,
                            bins=bins, cmap=cmap)[0]
        ax.set_xlabel("Longueur de la séquence de pile")
        ax.set_ylabel("Longueur de la séquence de face")
        ax.set_xlim([0, max_length])
        ax.set_ylim([0, max_length])
        ax.set_xticks(ticks)
        ax.set_yticks(ticks)
        ax.set_aspect('equal', 'box')
        suptitle = "Probabilité d'apparition du couple de séquences " + \
                   "les plus longues"
        fig.suptitle(suptitle, fontsize=14)
        self.set_title(ax)
        norm = mcolors.Normalize(vmin=0., vmax=histo2d.max()*1000)
        mappable = cm.ScalarMappable(norm=norm, cmap=cmap)
        cb = fig.colorbar(mappable)
        cb.ax.set_title("probabilité [\u2030]", fontsize=10)

        for i in ticks[1:]:
            for j in ticks[1:]:
                ax.text(i, j, f"{histo2d[i, j]*1000:.0f}", color='orangered',
                        horizontalalignment='center',
                        verticalalignment='center')

        return fig

    @staticmethod
    @jit(nopython=True)
    def count_squares(sq_length: int, sample: np.ndarray) -> int:
        """
        Return the number of 3x3 squares of sum 0, 1, 8 or 9 in sample
        """
        count = 0
        irange = range(sq_length - (sq_length % 3 + 1))
        for i in irange:
            for j in irange:
                square_3x3 = sample[i:i + 3, j:j + 3]
                square_sum = square_3x3.sum()
                if square_sum in (0, 1, 8, 9):
                    count += 1
        return count

    def compute_3x3_squares(self, ret_df=False, verbose=True):

        if verbose:
            print(f"Computing {self.nsample} samples for 3x3 squares")
        sq_length = int(np.sqrt(self.length))
        nsquares = np.empty((self.nsample, ), dtype=int)

        samples = progressbar(range(self.nsample)) if verbose \
            else range(self.nsample)

        for n in samples:
            # a random array of boolean for face or pile
            square_sample = np.random.choice(a=[0, 1],
                                             size=(sq_length, sq_length))
            nsquares[n] = self.count_squares(sq_length, square_sample)

        bins = np.arange(-0.5, nsquares.max() + 0.5, 1)
        histo, _ = np.histogram(nsquares, bins=bins, density=True)
        self.histo_nsquares = pd.DataFrame({'Probabilité 3x3': histo},
                                           index=(bins[:-1] + 0.5).astype(int))
        self.mean = nsquares.mean()
        self.std = nsquares.std()
        if ret_df:
            return pd.Series(nsquares,
                             name=f"{self.nsample} tirages aléatoires")

    def plot_3x3_squares(self, add_text=False):
        ax = self.histo_nsquares.plot.bar(rot=0, legend=False)
        self.set_title(ax)
        suptitle = "Histogramme des nombres de carrés 3x3"
        ax.get_figure().suptitle(suptitle, fontsize=14)
        ax.set_xlabel("Nombre de carrés")
        ax.set_ylabel("Probabilité")
        if add_text:
            vshift = self.histo_nsquares.max() / 100
            for i, row in self.histo_nsquares.iterrows():
                ax.text(i, row['Probabilité 3x3'] + vshift,
                        f"{row['Probabilité 3x3']:.4f}",
                        horizontalalignment='center')

        return ax.get_figure()


def print_square():
    """Print a 10x10 random square using html table"""

    def find_square(sample: np.ndarray) -> int:
        """
        Return the index of first 3x3 square of sum 0, 1, 8 or 9 in sample
        """
        count = 0
        irange = range(8)
        for i in irange:
            for j in irange:
                square_3x3 = sample[i:i + 3, j:j + 3]
                square_sum = square_3x3.sum()
                if square_sum in (0, 1, 8, 9):
                    return i, j
        return -1, -1

    position = -1, -1
    while position == (-1, -1):
        sample = np.random.choice(a=[0, 1], size=(10, 10))
        position = find_square(sample)

    i, j = position
    square_3x3 = sample[i:i + 3, j:j + 3]
    square_3x3_sum = square_3x3.sum()
    # A trick to mark the elements that belong to the 3x3 square
    square_3x3 -= 2

    output = '<meta charset="UTF-8">'
    output += f'<p><strong>Un carré 3 x 3 de somme {square_3x3_sum}' + \
              '</strong></p>'
    table = '<table>\n'
    for row in sample:
        table += '<tr>\n'
        for element in row:
            if element < 0:
                # the element is from the 3x3 square
                element = f'<strong>{element + 2}</strong>'
            table += f'<td>{element}</td>'
        table += '</tr>\n'
    table += '</table>\n'
    output += table
    display(html_print(output))


def plot_echantillon_hist(s: pd.Series, pof: PileOuFace, xmax, ax=None):
    """Superimpose df and pof histograms for echantillon"""
    if not ax:
        fig = plt.figure()
        ax = fig.add_subplot(111)

    bins = np.arange(start=-0.5, stop=xmax+1., step=1)
    title = f"Moyenne : {s.mean():.3f} - écart-type : {s.std():.3f}"
    axes = s.plot.hist(ax=ax,
                       bins=bins,
                       density=True,
                       ylim=(0., 0.5),
                       rwidth=0.95,
                       xticks=bins+0.5,
                       title=title,
                       label=s.name)
    axes.set_label(title)
    xlim = axes.get_xlim()
    axes = pof.histo_nsquares.plot(ax=ax, kind='bar', color='orange',
                                   alpha=0.75)
    axes.set_xlim(xlim)
    handle, legend = ax.get_legend_handles_labels()
    legend[1] = pof.name
    ax.legend(handle, legend)

    fig = axes.get_figure()
    fig.suptitle(s.name, fontsize=14)
    return axes.get_figure()


def generate_animation(nsample, nframe, large_pof, xmax):
    """return a nstep-random walk animation"""

    fig = plt.figure()
    ax = fig.add_subplot(111)

    def update(s):
        ax.clear()
        plot_echantillon_hist(s, large_pof, xmax, ax=ax)

    frames = (PileOuFace(nsample=nsample).compute_3x3_squares(ret_df=True,
                                                              verbose=False)
              for n in range(nframe))
    anim = animation.FuncAnimation(fig,
                                   func=update,
                                   frames=frames,
                                   interval=500,
                                   blit=False,
                                   repeat=False)

    return anim


if __name__ == '__main__':

    pof = PileOuFace()
    pof.compute_sequences()
    pof.plot1D()
    print('1D histogram:\n', pof.get_histogram1D())
    pof.plot2D()
    print('2D histogram:\n', pof.get_histogram2D())

    # 3x3 squares
    pof = PileOuFace(100000)
    pof.compute_3x3_squares()
    print(pof.histo_nsquares)
    pof.plot_3x3_squares()

    # Animation
    pof = PileOuFace(10000)
    pof.compute_3x3_squares()
    anim = generate_animation(10, 10, pof, xmax=11)
    anim

    plt.show()
